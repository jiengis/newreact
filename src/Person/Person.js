import React, { Component } from "react";
import Auto from "../Auto/Auto";

class Person extends Component {
  state = {
    auto: [
      { nome: "panda", marca: "fiat", colore: "red" },
      { nome: "uno", marca: "fiat", colore: "white" },
      { nome: "tipo", marca: "ford", colore: "black" }
    ]
  };

  render() {
    return (
      <div>
        <div>ciao sono una persona</div>
        <Auto marca={this.state.auto[0].marca} nome={this.state.auto[0].nome} />
        <Auto marca={this.state.auto[2].marca} nome={this.state.auto[2].nome} />
      </div>
    );
  }
}

export default Person;
